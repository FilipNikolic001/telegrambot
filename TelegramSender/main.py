import openpyxl
import requests
import random
import telethon.sync
from telethon.sync import TelegramClient
from notion.client import NotionClient

# Your Telethon API credentials
telethon_api_id = '21673381'
telethon_api_hash = '69f123787d83b6e529ed7a02d0f40d99'
telethon_session_name = 'SendingSesija'

# Your Notion API key and database URL
notion_api_key = 'secret_Y2NcDcMwCHEbU0xd2HSW2IMQ3nD7j7xKHT0IL7hEnCT'
notion_database_url = 'your_notion_database_url'

headers = {
    "Authorization": "Bearer " + notion_api_key,
    "Content-Type": "application/json",
    "Notion-Version": "2022-06-28",
}

# Create a Telethon client
telethon_client = TelegramClient(telethon_session_name, telethon_api_id, telethon_api_hash)

# URL to your Notion database
database_url = 'https://www.notion.so/cryptoposrednik/11b2e79a7fa940e6875c327c7cf521e2?v=e2469f692ab346fb81fe050a96c6c5f5&pvs=4'
databse_id = '11b2e79a7fa940e6875c327c7cf521e2'


def get_pages(num_pages=None):

    url = f"https://api.notion.com/v1/databases/{databse_id}/query"

    get_all = num_pages is None
    page_size = 100 if get_all else num_pages

    payload = {"page_size": page_size}
    response = requests.post(url, json=payload, headers=headers)

    data = response.json()

    # Comment this out to dump all data to a file
    # import json
    # with open('db.json', 'w', encoding='utf8') as f:
    #    json.dump(data, f, ensure_ascii=False, indent=4)

    results = data["results"]
    while data["has_more"] and get_all:
        payload = {"page_size": page_size, "start_cursor": data["next_cursor"]}
        url = f"https://api.notion.com/v1/databases/{databse_id}/query"
        response = requests.post(url, json=payload, headers=headers)
        data = response.json()
        results.extend(data["results"])


    return results



# Send message to username using Telethon
async def send_message_to_username(username, message):
    async with telethon_client:
        user_entity = await telethon_client.get_entity(username)
        await telethon_client.send_message(user_entity, message)


# Main function
async def main():
    await telethon_client.start()

    contacts = get_pages()
    for result in contacts:
        # Extracting data
        username = result['properties']['USERNAME']['title'][0]['plain_text']
        message_sent = result['properties']['MESSAGE_SENT']['checkbox']

        # Printing the extracted data
        print(f"Username: {username}")
        print("Message Sent:", "YES" if message_sent else "NO")



    random.shuffle(contacts)
    selected_usernames = contacts[:10]

    for username in selected_usernames:
        # Fetch the Notion database and find the corresponding entry
        collection_view = client.get_collection_view(database_url)
        for row in collection_view.collection.get_rows():
            if not row.MESSAGE_SENT:
                await send_message_to_username(username, "Your message here.")
                row.MESSAGE_SENT = True  # Mark as message sent
                break

    await telethon_client.disconnect()


if __name__ == "__main__":
    import asyncio

    asyncio.run(main())
