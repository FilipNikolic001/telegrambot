import asyncio
import openpyxl
from telethon.sync import TelegramClient
from openpyxl.worksheet.datavalidation import DataValidation

# Your API credentials
api_id = '21673381'
api_hash = '69f123787d83b6e529ed7a02d0f40d99'

# Your session name
session_name = 'ScrapinSesija'

# Initialize the Telegram client
client = TelegramClient(session_name, api_id, api_hash)

scraped_contacts = [
    { "username": "user1_username", "is_active": True},
    { "username": "user2_username", "is_active": False},
    # Add more contacts
]


async def scrape_group_contacts(group_username):
    # Create a new workbook and select the active sheet
    workbook = openpyxl.Workbook()
    sheet = workbook.active

    # Write header row
    header_row = ["Name", "Username"]
    sheet.append(header_row)

    await client.start()

    # Get the group entity
    entity = await client.get_entity(group_username)

    # Fetch participants from the group
    participants = await client.get_participants(entity)
    i =0;
    # Print the contacts
    for participant in participants:
        if participant.username:
            i+=1
            # New contact to append
            new_contact = { "username": participant.username}
            # Append the new contact to the list
            scraped_contacts.append(new_contact)
            print(f"Username: @{participant.username}")
    await client.disconnect()

    #dodaj sve u tabelu
    for contact in scraped_contacts:
        sheet.append([ contact["username"], ""])

    # Add checkboxes using data validation
    dv = DataValidation(
        type="list",
        formula1='"TRUE,FALSE"',
        showDropDown=True
    )
    # Apply data validation to the entire "Is Active" column except the header
    active_col = sheet["C"][1:]
    for cell in active_col:
        dv.add(cell)

    # Add data validation to the sheet
    sheet.add_data_validation(dv)

    # Save the workbook to a file
    workbook.save("scraped_contacts1.xlsx")

    print(f"Broj javljanja: @{i}")




group_username = 'ruskie_v_belgrade'
asyncio.run(scrape_group_contacts(group_username))



